#!/usr/bin/env ruby
require 'gitlab'
require 'pry'
require 'git'

Gitlab.endpoint = 'https://gitlab.com/api/v4'
# => "https://example.net/api/v4"
# set a user private token
Gitlab.private_token = 'XXXXXXXXX'
(1..8).each do | i |
  Gitlab.create_project(
    "Equipo-#{i}",
    :description => 'Repositorio equipo #{i} ',
    :namespace_id => 'XXXXX',
    :visibility => 'public',
    :merge_requests_enabled => '1',
    :issues_enabled => '1'
  )
end
